
exports.daysDifference = function (date1,date2){
	var one_day=1000*60*60*24;
	var date1_ms=date1.getTime();
	var date2_ms=date2.getTime();

	var difference_ms=date2_ms- date1_ms;

	difference_ms=difference_ms/1000;
	var seconds=Math.floor(difference_ms%60);
	difference_ms=difference_ms/60;
	var minutes=Math.floor(difference_ms%60);
	difference_ms=difference_ms/60;
	var hours=Math.floor(difference_ms%24);
	var days=Math.floor(difference_ms/24);
	return days+' days';
};



